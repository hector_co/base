﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Hco.Base.Utils
{
    public static class ObjectExtensions
    {
        private static ConcurrentDictionary<string, Tuple<Func<object, object>, Action<object, object>>> _accesors;

        static ObjectExtensions()
        {
            _accesors = new ConcurrentDictionary<string, Tuple<Func<object, object>, Action<object, object>>>();
        }

        public static TEntity CreateInstanceWithDefaultConstructor<TEntity>()
        {
            var ctorInfo = typeof(TEntity).GetTypeInfo().DeclaredConstructors.FirstOrDefault(c => c.GetParameters().Length == 0);
            if (ctorInfo == null)
                throw new Exception("Default constructor not found");
            return (TEntity)ctorInfo.Invoke(new object[] { });
        }

        private static void RegisterPropertyAccessor<TEntity>(string propertyName)
        {
            var propInfo = typeof(TEntity).GetTypeInfo().GetProperty(propertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            if (propInfo == null) throw new ArgumentException();

            _accesors.TryAdd(typeof(TEntity).Name + "_" + propertyName, new Tuple<Func<object, object>, Action<object, object>>(propInfo.GetValue, propInfo.SetValue));
        }

        private static void RegisterPropertyAccessor<TEntity, TValue>(Expression<Func<TEntity, TValue>> property)
        {
            PropertyInfo propInfo;
            if (!TryGetPropertyInfo(property, out propInfo))
                throw new ArgumentException();

            _accesors.TryAdd(typeof(TEntity).Name + "_" + propInfo.Name, new Tuple<Func<object, object>, Action<object, object>>(propInfo.GetValue, propInfo.SetValue));
        }

        private static void RegisterFieldAccessor<TEntity>(string fieldName)
        {
            var fieldInfo = typeof(TEntity).GetTypeInfo().GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);
            if (fieldInfo == null) throw new ArgumentException();

            _accesors.TryAdd(typeof(TEntity).Name + "_" + fieldName, new Tuple<Func<object, object>, Action<object, object>>(fieldInfo.GetValue, fieldInfo.SetValue));
        }

        private static void RegisterFieldAccessor<TEntity, TValue>(Expression<Func<TEntity, TValue>> field)
        {
            FieldInfo fieldInfo;
            if (!TryGetFieldInfo(field, out fieldInfo))
                throw new ArgumentException();

            _accesors.TryAdd(typeof(TEntity).Name + "_" + fieldInfo.Name, new Tuple<Func<object, object>, Action<object, object>>(fieldInfo.GetValue, fieldInfo.SetValue));
        }

        private static bool TryGetPropertyInfo<TEntity, TValue>(Expression<Func<TEntity, TValue>> property, out PropertyInfo propInfo)
        {
            var member = property.Body as MemberExpression;
            propInfo = member.Member as PropertyInfo;
            return propInfo != null;
        }

        private static bool TryGetFieldInfo<TEntity, TValue>(Expression<Func<TEntity, TValue>> field, out FieldInfo fieldInfo)
        {
            var member = field.Body as MemberExpression;
            fieldInfo = member.Member as FieldInfo;
            return fieldInfo != null;
        }

        public static object GetPropertyValue<TEntity>(this TEntity entity, string propertyName)
        {
            var key = typeof(TEntity).Name + "_" + propertyName;
            if (!_accesors.ContainsKey(key))
                RegisterPropertyAccessor<TEntity>(propertyName);
            return _accesors[key].Item1(entity);
        }

        public static void SetPropertyValue<TEntity>(this TEntity entity, string propertyName, object value)
        {
            var key = typeof(TEntity).Name + "_" + propertyName;
            if (!_accesors.ContainsKey(key))
                RegisterPropertyAccessor<TEntity>(propertyName);
            _accesors[key].Item2(entity, value);
        }

        public static object GetPropertyValue<TEntity, TValue>(this TEntity entity, Expression<Func<TEntity, TValue>> property)
        {
            PropertyInfo propInfo;
            if (!TryGetPropertyInfo(property, out propInfo))
                throw new ArgumentException();
            var key = typeof(TEntity).Name + "_" + propInfo.Name;
            if (!_accesors.ContainsKey(key))
                RegisterPropertyAccessor(property);
            return _accesors[key].Item1(entity);
        }

        public static void SetPropertyValue<TEntity, TValue>(this TEntity entity, Expression<Func<TEntity, TValue>> property, object value)
        {
            PropertyInfo propInfo;
            if (!TryGetPropertyInfo(property, out propInfo))
                throw new ArgumentException();
            var key = typeof(TEntity).Name + "_" + propInfo.Name;
            if (!_accesors.ContainsKey(key))
                RegisterPropertyAccessor(property);
            _accesors[key].Item2(entity, value);
        }

        public static object GetFieldValue<TEntity>(this TEntity entity, string fieldName)
        {
            var key = typeof(TEntity).Name + "_" + fieldName;
            if (!_accesors.ContainsKey(key))
                RegisterFieldAccessor<TEntity>(fieldName);
            return _accesors[key].Item1(entity);
        }

        public static void SetFieldValue<TEntity>(this TEntity entity, string fieldName, object value)
        {
            var key = typeof(TEntity).Name + "_" + fieldName;
            if (!_accesors.ContainsKey(key))
                RegisterFieldAccessor<TEntity>(fieldName);
            _accesors[key].Item2(entity, value);
        }

        public static object GetFieldValue<TEntity, TValue>(this TEntity entity, Expression<Func<TEntity, TValue>> field)
        {
            FieldInfo fieldInfo;
            if (!TryGetFieldInfo(field, out fieldInfo))
                throw new ArgumentException();
            var key = typeof(TEntity).Name + "_" + fieldInfo.Name;
            if (!_accesors.ContainsKey(key))
                RegisterFieldAccessor(field);
            return _accesors[key].Item1(entity);
        }

        public static void SetFieldValue<TEntity, TValue>(this TEntity entity, Expression<Func<TEntity, TValue>> field, object value)
        {
            FieldInfo fieldInfo;
            if (!TryGetFieldInfo(field, out fieldInfo))
                throw new ArgumentException();
            var key = typeof(TEntity).Name + "_" + fieldInfo.Name;
            if (!_accesors.ContainsKey(key))
                RegisterFieldAccessor(field);
            _accesors[key].Item2(entity, value);
        }
    }
}
