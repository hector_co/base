using System;
using Hco.Base.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Hco.Base.DataAccess.Tests
{
    [TestClass]
    public class ObjectExtensionsTests
    {
        const string TestString1 = "TestString1";
        const string TestString2 = "TestString2";
        const string TestString3 = "TestString3";
        const string TestString4 = "TestString4";
        const string TestString5 = "TestString5";

        [TestMethod]
        public void CreateObjectWithNonPublicConstructor()
        {
            var testObject = ObjectExtensions.CreateInstanceWithDefaultConstructor<TestObject>();

            testObject.ShouldNotBeNull();
        }

        [TestMethod]
        public void SetPropertyWithNonPublicSetterValue()
        {
            var testObject = new TestObject("");
            ObjectExtensions.SetPropertyValue(testObject, o => o.Property1, TestString1);
            ObjectExtensions.SetPropertyValue(testObject, o => o.Property2, TestString2);
            ObjectExtensions.SetPropertyValue(testObject, o => o.Property3, TestString3);

            testObject.Property1.ShouldBe(TestString1);
            testObject.Property2.ShouldBe(TestString2);
            testObject.Property3.ShouldBe(TestString3);
        }

        [TestMethod]
        public void SetNonPublicPropertyValue()
        {
            var testObject = new TestObject("");
            ObjectExtensions.SetPropertyValue(testObject, "Property4", TestString4);
            ObjectExtensions.SetPropertyValue(testObject, "Property5", TestString5);

            ObjectExtensions.GetPropertyValue(testObject, "Property4").ShouldBe(TestString4);
            ObjectExtensions.GetPropertyValue(testObject, "Property5").ShouldBe(TestString5);
        }
    }

    public class TestObject
    {
        private TestObject()
        {

        }

        public TestObject(string testValue)
        {

        }

        public string Property1 { get; protected set; }
        public string Property2 { get; private set; }
        public string Property3 { get; internal set; }
        private string Property4 { get; set; }
        internal string Property5 { get; set; }
    }
}
