﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Hco.Base.Domain
{
    public interface IUnitOfWork : IDisposable
    {
        Task BeginAsync(bool useGlobalTransaction = false, CancellationToken cancellationToken = default);
        Task RollbackAsync(CancellationToken cancellationToken = default);
        Task CommitAsync(bool resetGlobalTransaction = true, CancellationToken cancellationToken = default);
    }
}
