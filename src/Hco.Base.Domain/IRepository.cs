﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Hco.Base.Domain
{
    public interface IRepository<TAggregateRoot, TId>
        where TAggregateRoot : AggregateRoot<TId>
        where TId : IEquatable<TId>
    {
        TAggregateRoot GetById(TId id);
        Task<TAggregateRoot> GetByIdAsync(TId id, CancellationToken cancellationToken = default);
        void Save(TAggregateRoot aggregate);
        void Update(TAggregateRoot aggregate, bool ignoreVersion = false);
        int GetVersion(TId id);
    }
}
