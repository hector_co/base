﻿using System;

namespace Hco.Base.Domain
{
    public abstract class Entity<TId> : IEquatable<Entity<TId>>
        where TId : IEquatable<TId>
    {
        public virtual TId Id { get; protected set; }

        public bool Equals(Entity<TId> other)
        {
            if (other == null || GetType() != other.GetType()) return false;
            return Id.Equals(other.Id);
        }
    }
}
