﻿
namespace Hco.Base.Domain
{
    public abstract class SpecificationBase<TEntity> : ISpecification<TEntity>
    {
        public ISpecification<TEntity> And(ISpecification<TEntity> specification)
        {
            return new And<TEntity>(this, specification);
        }

        public ISpecification<TEntity> Or(ISpecification<TEntity> specification)
        {
            return new Or<TEntity>(this, specification);
        }

        public ISpecification<TEntity> Not()
        {
            return new Not<TEntity>(this);
        }

        public abstract bool IsSatisfiedBy(TEntity entity);

    }

    public class And<TEntity> : SpecificationBase<TEntity>
    {
        private readonly ISpecification<TEntity> _left;
        private readonly ISpecification<TEntity> _right;

        public And(ISpecification<TEntity> left, ISpecification<TEntity> right)
        {
            _left = left;
            _right = right;
        }

        public override bool IsSatisfiedBy(TEntity entity)
        {
            return _left.IsSatisfiedBy(entity) && _right.IsSatisfiedBy(entity);
        }
    }

    public class Or<TEntity> : SpecificationBase<TEntity>
    {
        private readonly ISpecification<TEntity> _left;
        private readonly ISpecification<TEntity> _right;

        public Or(ISpecification<TEntity> left, ISpecification<TEntity> right)
        {
            _left = left;
            _right = right;
        }

        public override bool IsSatisfiedBy(TEntity entity)
        {
            return _left.IsSatisfiedBy(entity) || _right.IsSatisfiedBy(entity);
        }
    }

    public class Not<TEntity> : SpecificationBase<TEntity>
    {
        private readonly ISpecification<TEntity> _spec;

        public Not(ISpecification<TEntity> specification)
        {
            _spec = specification;
        }

        public override bool IsSatisfiedBy(TEntity entity)
        {
            return !_spec.IsSatisfiedBy(entity);
        }
    }
}
