﻿using System;
using System.Collections.Generic;

namespace Hco.Base.Domain
{
    public abstract class AggregateRootWithEvents<TId> : AggregateRoot<TId>
        where TId : IEquatable<TId>
    {
        private readonly List<AggregateEvent<TId>> _events;

        protected AggregateRootWithEvents() : base()
        {
            _events = new List<AggregateEvent<TId>>();
        }

        public void MarkAsCommited()
        {
            _events.Clear();
        }

        public IEnumerable<AggregateEvent<TId>> GetUncommittedEvents()
        {
            return _events;
        }

        protected void Dispatch<TEvent>(TEvent @event) where TEvent : AggregateEvent<TId>
        {
            _events.Add(@event);
        }

        public void LoadFromHistory(IEnumerable<AggregateEvent<TId>> history)
        {
            foreach (var @event in history)
            {
                ApplyChange(@event, false);
            }
        }

        protected void ApplyChange(AggregateEvent<TId> @event)
        {
            @event.AggregateId = Id;
            @event.AggregateVersion++;
            ApplyChange(@event, true);
        }

        private void ApplyChange(AggregateEvent<TId> @event, bool isNew)
        {
            Version = @event.AggregateVersion;
            Apply(@event);
            if (isNew) _events.Add(@event);
        }

        protected abstract void Apply<TEvent>(TEvent @event) where TEvent : AggregateEvent<TId>;
    }
}
