﻿using MediatR;
using System;

namespace Hco.Base.Domain
{
    public abstract class AggregateEvent<TAggregateId> : INotification
    {
        public AggregateEvent()
        {
            Guid = Guid.NewGuid();
            DateTime = DateTimeOffset.UtcNow;
        }

        public Guid Guid { get; set; }
        public DateTimeOffset DateTime { get; set; }

        public TAggregateId AggregateId { get; set; }
        public int AggregateVersion { get; set; }
    }
}
