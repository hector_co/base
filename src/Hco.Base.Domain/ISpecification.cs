﻿
namespace Hco.Base.Domain
{
    public interface ISpecification<TEntity>
    {
        bool IsSatisfiedBy(TEntity entity);
        ISpecification<TEntity> And(ISpecification<TEntity> specification);
        ISpecification<TEntity> Or(ISpecification<TEntity> specification);
        ISpecification<TEntity> Not();
    }

    public interface IAndSpecification<TEntity> : ISpecification<TEntity>
    {
    }

    public interface IOrSpecification<TEntity> : ISpecification<TEntity>
    {
    }

    public interface INotSpecification<TEntity> : ISpecification<TEntity>
    {
    }
}
