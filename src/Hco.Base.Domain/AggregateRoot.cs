﻿using System;

namespace Hco.Base.Domain
{
    public abstract class AggregateRoot<TId> : Entity<TId>
        where TId : IEquatable<TId>
    {
        protected AggregateRoot()
        {
            Version = 1;
        }

        public int Version { get; set; }
    }
}
