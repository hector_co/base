﻿using System;

namespace Hco.Base.Domain.Exceptions
{
    public class AggregateNotFoundException : DomainException
    {
        public AggregateNotFoundException()
        {
        }

        public AggregateNotFoundException(string message) : base(message)
        {
        }

        public AggregateNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}