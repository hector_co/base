﻿
using System;

namespace Hco.Base.Domain.Exceptions
{
    public class AggregateVersionException : DomainException
    {
        public AggregateVersionException()
        {
        }

        public AggregateVersionException(string message)
            : base(message)
        {
        }

        public AggregateVersionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
