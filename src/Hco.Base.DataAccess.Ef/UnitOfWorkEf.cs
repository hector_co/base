﻿using System;
using System.Collections.Generic;
using Hco.Base.Domain;
using Microsoft.EntityFrameworkCore;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using System.Linq;
using Microsoft.EntityFrameworkCore.Storage;

namespace Hco.Base.DataAccess.Ef
{
    public class UnitOfWorkEf<TContext> : IUnitOfWork
        where TContext : DbContext
    {
        private bool _inTransaction;
        private IDbContextTransaction _transaction;
        private bool _processingCommit;
        private Dictionary<object, List<Action>> _trackedAggregates;
        private Dictionary<object, List<Action>> _secondaryTrackedAggregates;
        private readonly ILifetimeScope _scope;
        protected readonly IMediator _mediator;

        public UnitOfWorkEf(ILifetimeScope scope, IMediator mediator)
        {
            _scope = scope;
            _mediator = mediator;
            _inTransaction = false;
            _processingCommit = false;
            _trackedAggregates = new Dictionary<object, List<Action>>();
            _secondaryTrackedAggregates = new Dictionary<object, List<Action>>();
        }

        public TContext CurrentContext { get; private set; }

        public virtual void Dispose()
        {
            _transaction?.Dispose();
            CurrentContext?.Dispose();
        }

        public virtual async Task BeginAsync(bool useGlobalTransaction = false, CancellationToken cancellationToken = default)
        {
            if (_inTransaction) return;
            CurrentContext = _scope.BeginLifetimeScope().Resolve<TContext>();
            if (useGlobalTransaction) _transaction = await CurrentContext.Database.BeginTransactionAsync(cancellationToken);
            _inTransaction = true;
        }

        public virtual async Task RollbackAsync(CancellationToken cancellationToken = default)
        {
            if (!_inTransaction) return;
            if (_transaction != null)
                await _transaction?.RollbackAsync(cancellationToken);
            _transaction?.Dispose();
            _trackedAggregates.Clear();
            CurrentContext.Dispose();
            _inTransaction = false;
        }

        public virtual void TrackAggregate<TId>(AggregateRoot<TId> aggregateRoot, Action postAction = null)
            where TId : IEquatable<TId>
        {
            if (!_inTransaction) return;
            var aggregatesList = _processingCommit ? _secondaryTrackedAggregates : _trackedAggregates;
            if (!aggregatesList.ContainsKey(aggregateRoot))
            {
                aggregatesList.Add(aggregateRoot, new List<Action>());
            }
            if (postAction != null)
                aggregatesList[aggregateRoot].Add(postAction);
        }

        public virtual bool UntrackAggregate<TId>(AggregateRoot<TId> aggregateRoot)
            where TId : IEquatable<TId>
        {
            if (!_inTransaction) return false;
            return _trackedAggregates.ContainsKey(aggregateRoot) && _trackedAggregates.Remove(aggregateRoot);
        }

        public virtual async Task CommitAsync(bool resetGlobalTransaction = true, CancellationToken cancellationToken = default)
        {
            if (!_inTransaction) return;
            _processingCommit = true;
            await CurrentContext.SaveChangesAsync(cancellationToken);

            foreach (var trackedAggregate in _trackedAggregates)
            {
                foreach (var action in trackedAggregate.Value)
                {
                    action.Invoke();
                }
                await ProcessAggregateAsync(trackedAggregate.Key as dynamic);
            }

            if (_secondaryTrackedAggregates.Any())
            {
                _trackedAggregates = _secondaryTrackedAggregates;
                _secondaryTrackedAggregates = new Dictionary<object, List<Action>>();
                await CommitAsync(resetGlobalTransaction, cancellationToken);
                return;
            }

            _trackedAggregates.Clear();
            if (resetGlobalTransaction)
            {
                _transaction?.Commit();
                _transaction?.Dispose();
                CurrentContext.Dispose();
                _inTransaction = false;
            }
            _processingCommit = false;
        }

        protected virtual async Task ProcessAggregateAsync<TId>(AggregateRoot<TId> aggregate)
            where TId : IEquatable<TId>
        {
            if (aggregate is AggregateRootWithEvents<TId> aggWithEvents)
            {
                foreach (var @event in aggWithEvents.GetUncommittedEvents())
                {
                    @event.AggregateId = aggWithEvents.Id;
                    await _mediator.Publish(@event);
                }
                aggWithEvents.MarkAsCommited();
            }
        }
    }
}
