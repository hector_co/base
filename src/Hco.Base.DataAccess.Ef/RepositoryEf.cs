﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Hco.Base.Domain;
using Hco.Base.Domain.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Hco.Base.DataAccess.Ef
{
    public abstract class RepositoryEf<TContext, TAggregateRoot, TId> : IRepository<TAggregateRoot, TId>
        where TAggregateRoot : AggregateRoot<TId>
        where TContext : DbContext
        where TId : IEquatable<TId>
    {
        protected RepositoryEf(UnitOfWorkEf<TContext> unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        protected UnitOfWorkEf<TContext> UnitOfWork { get; set; }

        public virtual TAggregateRoot GetById(TId id)
        {
            var ctx = UnitOfWork.CurrentContext;
            return ctx.Set<TAggregateRoot>().AsNoTracking().FirstOrDefault(a => a.Id.Equals(id));
        }

        public virtual async Task<TAggregateRoot> GetByIdAsync(TId id, CancellationToken cancellationToken = default)
        {
            var ctx = UnitOfWork.CurrentContext;
            return await ctx.Set<TAggregateRoot>().AsNoTracking().FirstOrDefaultAsync(a => a.Id.Equals(id));
        }

        public virtual int GetVersion(TId id)
        {
            var ctx = UnitOfWork.CurrentContext;
            return ctx.Set<TAggregateRoot>().Where(a => a.Id.Equals(id)).Select(a => a.Version).FirstOrDefault();
        }

        public virtual void Save(TAggregateRoot aggregate)
        {
            var ctx = UnitOfWork.CurrentContext;
            ctx.Set<TAggregateRoot>().Add(aggregate);
            UnitOfWork.TrackAggregate(aggregate);
        }

        public virtual void Update(TAggregateRoot aggregate, bool ignoreVersion = false)
        {
            var ctx = UnitOfWork.CurrentContext;
            var aggVersion = GetVersion(aggregate.Id);

            if (!ignoreVersion)
            {
                if (aggregate is AggregateRootWithEvents<TId> aggWithEvents)
                {
                    if (aggVersion + aggWithEvents.GetUncommittedEvents().Count() != aggregate.Version)
                        throw new AggregateVersionException();
                }
                else
                {
                    if (aggVersion != aggregate.Version)
                        throw new AggregateVersionException();
                }
            }

            if (!(aggregate is AggregateRootWithEvents<TId>))
                aggregate.Version++;

            ctx.Set<TAggregateRoot>().Update(aggregate);
            UnitOfWork.TrackAggregate(aggregate);
        }
    }
}