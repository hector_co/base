﻿namespace Hco.Base.Domain.Tests
{
    public class User : AggregateRootWithEvents<int>
    {
        private string _name;
        private string _lastName;
        private string _userName;
        private string _password;
        private bool _active;
        private string _avatar;

        protected User()
        {
        }

        public User(string name, string lastName, string userName, string password, string avatar)
        {
            ApplyChange(new RegisteredUser
            {
                Name = name,
                LastName = lastName,
                UserName = userName,
                Password = password,
                Avatar = avatar
            });
        }

        public void Activate()
        {
            ApplyChange(new ActivatedUser());
        }

        public void Deactivate()
        {
            ApplyChange(new DeactivatedUser());
        }

        private void Apply(RegisteredUser @event)
        {
            _name = @event.Name;
            _lastName = @event.LastName;
            _userName = @event.UserName;
            _password = @event.Password;
            _avatar = @event.Avatar;
            _active = false;
        }

        private void Apply(ActivatedUser @event)
        {
            _active = true;
        }

        private void Apply(DeactivatedUser @event)
        {
            _active = false;
        }

        protected override void Apply<TEvent>(TEvent @event)
        {
            Apply(@event as dynamic);
        }
    }

    public class ActivatedUser : AggregateEvent<int>
    {
    }

    public class DeactivatedUser : AggregateEvent<int>
    {
    }

    public class RegisteredUser : AggregateEvent<int>
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Avatar { get; set; }
    }
}
